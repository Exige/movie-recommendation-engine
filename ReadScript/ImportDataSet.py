import csv
import os
import datetime
import sys, getopt

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hi:")
    except getopt.GetoptError:
        print('ImportDataSet.py -i <any cassandra host in cluster>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('ImportDataSet.py -i <any cassandra host in cluster>')
            sys.exit()
        elif opt in ("-i"):
            cluster_ip = arg

    from cassandra.cluster import Cluster

    cluster = Cluster([cluster_ip])
    session = cluster.connect()
    setupDB(session)


    rownum = 0

    # Insert movies.
    with open(os.path.dirname(os.path.abspath(__file__)) + '/ml-20m/movies.csv') as f:
        reader = csv.reader(f)
        for row in reader:
            if(rownum > 0):
                session.execute('INSERT INTO movies(movieId, title, genre) VALUES (%s, %s, %s)', (int(row[0]),row[1], row[2]))
                print("%s entries processed" % str(rownum+1))
            rownum += 1


    rownum = 0
    with open(os.path.dirname(os.path.abspath(__file__)) + '/ml-20m/ratings.csv') as f:
        reader = csv.reader(f)
        for row in reader:
            if (rownum > 0 and rownum < 10000):
                if(float(row[2]) < 2.5):
                    score = -1
                else:
                    score = 1

                session.execute('INSERT INTO ratings(userId, movieId, likes) VALUES (%s, %s, %s)',
                                (int(row[0]), int(row[1]), int(score)))
                print("%s entries processed" % str(rownum  + 1))
            rownum += 1




def setupDB(session):
    session.execute('CREATE KEYSPACE IF NOT EXISTS movies_db WITH REPLICATION = { \'class\' : \'NetworkTopologyStrategy\', \'datacenter1\' : 1 };')
    session.set_keyspace('movies_db')
    session.execute('CREATE TABLE IF NOT EXISTS movies(movieId int, title varchar, genre varchar, PRIMARY KEY((movieId)));')
    session.execute('CREATE TABLE IF NOT EXISTS ratings(userId int, movieId int, likes int, PRIMARY KEY((userid, movieid)));')
    session.execute('CREATE TABLE IF NOT EXISTS movieScores(movieId1 int, movieId2 int, score int, PRIMARY KEY((movieId1, movieId2)));')


if __name__ == "__main__":
   main(sys.argv[1:])


