from pyspark import SparkContext, SparkConf



conf = SparkConf().setAppName("test app").setMaster("spark://138.68.122.133:7077")
#conf.set("spark.driver.port", "20002" ).set("spark.rpc.askTimeout", "600s").set("SPARK_LOCAL_IP", "10.133.29.26")
#conf.set("spark.driver.bindAddress", "10.133.29.26").set("spark.driver.host", "10.133.29.26")
#conf.set("spark.blockManager.port", "46161")
#conf.set("spark.local.ip","0.0.0.0")
#conf.set("spark.executor.memory", "512mb")

sc = SparkContext(conf=conf)


nums = range(5000)

print(sc.parallelize(nums).sum())
