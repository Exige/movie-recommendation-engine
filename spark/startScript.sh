#! /bin/bash

if [ $1 = "master" ]; then
    $SPARK_HOME/bin/spark-class org.apache.spark.deploy.master.Master -h "${SPARK_LOCAL_IP}"  --webui-port 8801
else
    $SPARK_HOME/bin/spark-class org.apache.spark.deploy.worker.Worker "spark://${SPARK_MASTER_IP}:7077" --webui-port 8802 
fi

