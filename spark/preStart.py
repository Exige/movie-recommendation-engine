#! /usr/bin/python

import etcd, os, sys

client = etcd.Client(host=os.environ['ETCD_ENDPOINT'], port=2379)
ident = os.environ['IDENT']

if sys.argv[1] == 'master':
    try:
        master = client.read('/services/spark/master').value
        print(master)
    except etcd.EtcdKeyNotFound:
        print("key not found!")
        # master = client.read('/services/spark-master', wait=True).value
        exit(-1)

if sys.argv[1] == 'worker':
    try:
        master = client.read('/services/spark/master').value
        print(master)
    except etcd.EtcdKeyNotFound:
        # master = client.read('/services/spark-master', wait=True).value
        exit(-1)