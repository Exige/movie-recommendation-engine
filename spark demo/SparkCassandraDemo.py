from pyspark import SparkContext, SparkConf
from pyspark import SQLContext
import sys
from pyspark.sql.functions import lit



def main(argv):
    master = argv[0]
    cassandra_cluster = argv[1]
    conf = SparkConf().setAppName("Simple App").setMaster(master).set("spark.cassandra.connection.host", cassandra_cluster)\
        .set("spark.driver.port", "20002" ).set("spark.rpc.askTimeout", "600s") \
        .set("spark.driver.bindAddress", "10.0.2.15").set("spark.driver.host", "10.0.2.15").set("spark.blockManager.port", "46161")
    sc = SparkContext(conf=conf)

    sqlContext = SQLContext(sc)
    df1 = sqlContext.read.format("org.apache.spark.sql.cassandra").options(table="ratings", keyspace="movies_db").load()
    df2 = sqlContext.read.format("org.apache.spark.sql.cassandra").options(table="ratings", keyspace="movies_db").load()

    df1.createOrReplaceTempView("df1")
    df2.createOrReplaceTempView("df2")

    base = sqlContext.sql("SELECT r.movieId AS movieid1, s.movieId AS movieid2, s.likes AS likes1, r.likes AS likes2 FROM df1 r JOIN df2 s ON r.userid = s.userid")
    base.show()
    base.createOrReplaceTempView("base")

    # Cases where the user likes both movies
    sqlDF = sqlContext.sql("SELECT movieid1, movieid2 FROM base WHERE likes1 = 1 AND likes2 = 1")
    sqlDF = sqlDF.withColumn("score", lit(1))

    sqlDF.show()
    sqlDF.createOrReplaceTempView("sqlDF")

    sqlDF2 = sqlContext.sql("SELECT movieid1, movieid2 FROM base WHERE likes1 = -1 AND likes2 = -1")
    sqlDF2 = sqlDF2.withColumn("score", lit(-1))
    sqlDF2.show()
    sqlDF2.createOrReplaceTempView("sqlDF2")

    sqlDF3 = sqlContext.sql("SELECT movieid1, movieid2 FROM base WHERE likes1 = 1 AND likes2 = -1")
    sqlDF3 = sqlDF3.withColumn("score", lit(-1))
    sqlDF3.show()
    sqlDF3.createOrReplaceTempView("sqlDF3")

    sqlDF4 = sqlContext.sql("SELECT movieid1, movieid2 FROM base WHERE likes1 = -1 AND likes2 = 1")
    sqlDF4 = sqlDF4.withColumn("score", lit(0))
    sqlDF4.show()
    sqlDF4.createOrReplaceTempView("sqlDF4")

    sqlDF = sqlDF.union(sqlDF2).union(sqlDF3).union(sqlDF4)
    sqlDF.show()
    sqlDF.createOrReplaceTempView("sqlDF")
    result = sqlContext.sql("SELECT  movieid1, movieid2, sum(score) AS score FROM sqlDF GROUP BY movieId1, movieId2")
    result.show()
    result.write.format("org.apache.spark.sql.cassandra").mode('overwrite').options(table="moviescores", keyspace="movies_db").save()


if __name__ == "__main__":
   main(sys.argv[1:])
