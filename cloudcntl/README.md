# Cloud control
The scripts in this folder are designed to control the cloud. 

## dependancies
In order to install the dependancies run the following command:
```
pip install -r requirements.txt
```

## access token
In order to use the DigitalOcean API you need a secrit access token. This token can be generated on the API page of your DigitalOcean account. Add a file *token.txt* to the directory, e.g.:
```
echo "42747330e9c4b8eefa9[...]" > token.txt
```
Since this token should remain secret, give it the proper access rights, like your ssh private key:
```
chmod 600 token.txt
```


## fleetctl
Append a few lines to your *.bashrc* file to prevent some awful errors:
```
echo 'eval $(ssh-agent)' >> ~/.bashrc
echo 'ssh-add' >> ~/.bashrc
```
export one of the cluster ip addresses so it can be used as a fleet tunnel. Preferably use a floating IP for this. Add it to your *bash_profile* for convenience reasons:
```
echo 'export FLEETCTL_TUNNEL=188.11...' >> ~/.bashrc
```

Source your bash profile:
```
source ~/.bashrc
```

Install fleetctl:
```
apt install fleet
```
Now you are ready to go.
