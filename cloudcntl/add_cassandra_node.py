import os
import digitalocean
from jinja2 import Environment, FileSystemLoader
import requests
from time import sleep


# read the secret token
with open('token.txt', 'r') as f:
    token = f.read().splitlines()[0]


# Get the discovery URL.
with open("last_discovery_url.txt", "r") as text_file:
    r = text_file.read()

# get ssh keys
manager = digitalocean.Manager(token=token)
keys = manager.get_all_sshkeys()


# render cloud-config for the slaves
env = Environment(loader=FileSystemLoader('./'))
cloud_config_template = env.get_template('cloud-config')
cloud_config_cassandra_slave = cloud_config_template.render(metadata="cassandra=slave", discovery_url = r)
print(r)

# create 2 Cassandra slave machines
print("Adding cassandra node")
cassandra_node = digitalocean.Droplet.create_multiple(token=token,
                               names=['cassandra_slave_new'],
                               region='ams3', # Amster
                               image='coreos-stable',
                               private_networking=True,
                               size_slug='2gb',  # 1024MB
                               ssh_keys=keys, #Automatic conversion
                               backups=False,
                               user_data=cloud_config_cassandra_slave,
                                     )


# store the id of the master droplet
master_id = cassandra_node[0].id

machine = manager.get_droplet(master_id)
 
while machine.status != "active":  # wait for the machine to get active
    print("waiting for machine to get active")
    sleep(2)
    machine = manager.get_droplet(master_id)

print("Done")


