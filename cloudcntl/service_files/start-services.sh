#!/bin/bash

fleetctl start cassandra-master.service
fleetctl start spark-master.service
fleetctl start cassandra-slave.service
fleetctl start spark-slave.service

