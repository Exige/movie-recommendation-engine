import digitalocean
from os import remove
from os.path import expanduser

# read the secret token
with open('token.txt', 'r') as f:
    token = f.read().splitlines()[0]

manager = digitalocean.Manager(token=token)

my_droplets = manager.get_all_droplets()
for droplet in my_droplets:
    print("destroying:", droplet.name)
    droplet.destroy()

# remove known hosts file for fleet, because it will complain about mitm-attacks etc, scary shit.
# this is however expected behaviour because we assign a known ip to a different host with new keys etc
try:
    remove(expanduser("~") + "/.fleetctl/known_hosts")
except FileNotFoundError:
    pass  # file does not exists, which is fine

print("done")
