import digitalocean
from jinja2 import Environment, FileSystemLoader
import requests
from time import sleep


# read the secret token
with open('token.txt', 'r') as f:
    token = f.read().splitlines()[0]

# get a new discovery url for etcd
r = requests.get('https://discovery.etcd.io/new?size=3')

# render a cloud-config
env = Environment(loader=FileSystemLoader('./'))
cloud_config_template = env.get_template('cloud-config')
cloud_config = cloud_config_template.render(metadata="spark=slave", discovery_url = r.text)

manager = digitalocean.Manager(token=token)
keys = manager.get_all_sshkeys()

# create 3 machines
print("creating cluster")
digitalocean.Droplet.create_multiple(token=token,
                               names=['s3', 's4', 's5', 's6'],
                               region='ams3', # Amster
                               image='coreos-stable',
                               private_networking=True,
                               size_slug='1gb',  # 512MB
                               ssh_keys=keys, #Automatic conversion
                               backups=False,
                               user_data=cloud_config,
                                     )

# assign a floating ip to one of the machines, which makes working with fleet easier
floating_ip = manager.get_all_floating_ips()[0]
machines = manager.get_all_droplets()
print("assigning floating ip:", floating_ip.ip, "to", machines[0].name)

while machines[0].status != "active":  # wait for the machine to get active
    print("waiting for machine to get active")
    sleep(2)
    machines = manager.get_all_droplets()

floating_ip.assign(machines[0].id)

print("done")