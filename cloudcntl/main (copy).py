import digitalocean
from jinja2 import Environment, FileSystemLoader
import requests
from time import sleep


# read the secret token
with open('token.txt', 'r') as f:
    token = f.read().splitlines()[0]

# get a new discovery url for etcd and store it so we can add new machines to the initial cluster
r = requests.get('https://discovery.etcd.io/new?size=3')
with open("last_discovery_url.txt", "w") as text_file:
    text_file.write(r.text)

# get ssh keys
manager = digitalocean.Manager(token=token)
keys = manager.get_all_sshkeys()

# render clout-config for the slaves
env = Environment(loader=FileSystemLoader('./'))
cloud_config_template = env.get_template('cloud-config')
cloud_config_spark_slave = cloud_config_template.render(metadata="spark=slave", discovery_url = r.text)

# create 2 slave machines
print("creating spark cluster")
digitalocean.Droplet.create_multiple(token=token,
                               names=['spark_slave1', 'spark_slave2', 'spark_slave3', 'spark_slave4'],
                               region='ams3', # Amster
                               image='coreos-stable',
                               private_networking=True,
                               size_slug='2gb',  # 1024MB
                               ssh_keys=keys, #Automatic conversion
                               backups=False,
                               user_data=cloud_config_spark_slave,
                                     )

cloud_config_spark_master = cloud_config_template.render(metadata="spark=master", discovery_url = r.text)

# create a master
master = digitalocean.Droplet.create_multiple(token=token,
                               names=['spark_master1'],
                               region='ams3', # Amster
                               image='coreos-stable',
                               private_networking=True,
                               size_slug='2gb',  # 1024MB
                               ssh_keys=keys, #Automatic conversion
                               backups=False,
                               user_data=cloud_config_spark_master,
                                     )

# store the id of the master droplet
spark_master_id = master[0].id

# assign a floating ip to the master machine, which makes working with fleet easier / public web interface etc.
floating_ip = manager.get_all_floating_ips()[0]
machine = manager.get_droplet(spark_master_id)
print("assigning floating ip:", floating_ip.ip, "to", machine.name)

while machine.status != "active":  # wait for the machine to get active
    print("waiting for machine to get active")
    sleep(2)
    machine = manager.get_droplet(spark_master_id)

floating_ip.assign(machine.id)

print("done")


################################################
########## Start Cassandra cluster #############
################################################



# render cloud-config for the slaves
env = Environment(loader=FileSystemLoader('./'))
cloud_config_template = env.get_template('cloud-config')
cloud_config_cassandra_slave = cloud_config_template.render(metadata="cassandra=slave", discovery_url = r.text)

# create 2 Cassandra slave machines
print("creating Cassandra cluster")
digitalocean.Droplet.create_multiple(token=token,
                               names=['cassandra_slave1', 'cassandra_slave2', 'cassandra_slave3', 'cassandra_slave4'],
                               region='ams3', # Amster
                               image='coreos-stable',
                               private_networking=True,
                               size_slug='2gb',  # 1024MB
                               ssh_keys=keys, #Automatic conversion
                               backups=False,
                               user_data=cloud_config_cassandra_slave,
                                     )

cloud_config_cassandra_master = cloud_config_template.render(metadata="cassandra=master", discovery_url = r.text)

# create a master
cassandra_master = digitalocean.Droplet.create_multiple(token=token,
                               names=['cassandra_master1'],
                               region='ams3', # Amster
                               image='coreos-stable',
                               private_networking=True,
                               size_slug='2gb',  # 1024MB
                               ssh_keys=keys, #Automatic conversion
                               backups=False,
                               user_data=cloud_config_cassandra_master,
                                     )

# store the id of the master droplet
master_id = cassandra_master[0].id

# assign a floating ip to the master machine, which makes working with fleet easier / public web interface etc.
floating_ip = manager.get_all_floating_ips()[1]
machine = manager.get_droplet(master_id)
print("assigning floating ip:", floating_ip.ip, "to", machine.name)
 
while machine.status != "active":  # wait for the machine to get active
    print("waiting for machine to get active")
    sleep(2)
    machine = manager.get_droplet(master_id)

floating_ip.assign(machine.id)

print("done")


